<?php

use yii\db\Migration;

/**
 * Handles dropping username from table `user`.
 */
class m160507_161849_drop_username_from_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropIndex('username', 'user');
        $this->alterColumn('user', 'username', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        echo "m160507_161849_drop_username_from_user cannot be reverted.\n";

        return false;
    }
}
