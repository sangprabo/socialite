<?php

use yii\db\Migration;

/**
 * Handles adding name to table `user`.
 */
class m160507_064720_add_name_to_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $table_name = 'user';
        $this->addColumn($table_name, 'name', 'VARCHAR(255) AFTER `username` ');
        $this->addColumn($table_name, 'surname', 'VARCHAR(255) AFTER `name` ');
        $this->addColumn($table_name, 'dob', 'DATE NOT NULL DEFAULT "1970-01-01" AFTER `email` ');
        $this->addColumn($table_name, 'picture_filename', 'VARCHAR(255) AFTER `status` ');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $table_name = 'user';
        $this->dropColumn($table_name, 'name');
        $this->dropColumn($table_name, 'surname');
        $this->dropColumn($table_name, 'dob');
        $this->dropColumn($table_name, 'picture_filename');
    }
}
