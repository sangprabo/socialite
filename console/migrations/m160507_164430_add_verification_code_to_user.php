<?php

use yii\db\Migration;

/**
 * Handles adding verification_code to table `user`.
 */
class m160507_164430_add_verification_code_to_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'verification_code', $this->string() . ' AFTER `password_reset_token`');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'verification_code');
    }
}
