<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$verificationLink = Yii::$app->urlManager->createAbsoluteUrl(['site/verify', 'verificationCode' => $user->verification_code]);
?>
<div class="verify-account">
    <p>Hello <?= Html::encode($user->name) ?>,</p>

    <p>Follow the link below to activate your account:</p>

    <p><?= Html::a(Html::encode($verificationLink), $verificationLink) ?></p>
</div>
