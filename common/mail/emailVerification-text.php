<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$verificationLink = Yii::$app->urlManager->createAbsoluteUrl(['site/verify', 'verificationCode' => $user->verification_code]);
?>
Hello <?= $user->name?>,

Follow the link below to reset your password:

<?= $verificationLink ?>
