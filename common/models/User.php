<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\web\UploadedFile;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $name
 * @property string $surname
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property string $dob
 * @property string $picture_filename
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_PENDING = 9;
    const STATUS_ACTIVE = 10;

    const ROLE_ADMIN = 'admin';
    const ROLE_USER  = 'user';

    public $password;
    public $picture;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'name', 'surname', 'auth_key', 'password_hash', 'dob'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED, self::STATUS_PENDING]],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            [['username', 'name', 'surname', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['role'], 'string', 'max' => 50],
            [['username'], 'unique'],
            [['email'], 'unique'],
            ['email', 'email'],
            ['username', 'string', 'min' => 4],
            [['name', 'surname'], 'string', 'min' => 3],
            [['password_reset_token'], 'unique'],
            ['password', 'string', 'min' => 6],
            [['dob'], 'safe'],
            [['dob'], 'date', 
                'format' => 'yyyy-M-d',
                'max' => date('Y-m-d', strtotime('-13 year')), 
                'message' => 'The format for Date of Birth is yyyy-mm-dd (eg : 1980-12-25)',
                'tooBig' => 'Age must be older than 13 years'],
            [['picture_filename'], 'string'],
            [['picture'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                   => Yii::t('app', 'ID'),
            'username'             => Yii::t('app', 'Username'),
            'name'                 => Yii::t('app', 'Name'),
            'surname'              => Yii::t('app', 'Surname'),
            'auth_key'             => Yii::t('app', 'Auth Key'),
            'password_hash'        => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'email'                => Yii::t('app', 'Email'),
            'dob'                  => Yii::t('app', 'Date of Birth'),
            'role'                 => Yii::t('app', 'Role'),
            'status'               => Yii::t('app', 'Status'),
            'picture_filename'     => Yii::t('app', 'Picture'),
            'created_at'           => Yii::t('app', 'Created At'),
            'updated_at'           => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by username or email
     * @param  string $usernameOrEmail username or email
     * @return static|null                  
     */
    public static function findByUsernameOrEmail($usernameOrEmail)
    {
        // if a valid email, check user's email
        if (filter_var($usernameOrEmail, FILTER_VALIDATE_EMAIL))
            return static::findOne(['status' => self::STATUS_ACTIVE, 'email' => $usernameOrEmail]);
        
        // else, get user by username
        return static::findByUsername($usernameOrEmail);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Generate new verification code to activate the account
     */
    public function generateVerificationCode()
    {
        $this->verification_code = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Get `role` field as array
     * @return array array of user's roles
     */
    public static function getRoleAsArray()
    {
        return [
            static::ROLE_USER => 'User',
            static::ROLE_ADMIN => 'Admin',
            ];
    }

    /**
     * Get `status` field as array
     * @return array array of user's status
     */
    public static function getStatusAsArray()
    {
        return [
            static::STATUS_ACTIVE  => 'Active', 
            static::STATUS_DELETED => 'Deleted',
            static::STATUS_PENDING => 'Pending',
        ];
    }

    /**
     * Upload the profile picture and save the generated filename
     * @param string $filename generated filename
     * @return boolean
     */
    public function upload($filename)
    {
        if ($this->validate()) 
        {
            $this->picture->saveAs('uploads/' . $filename);

            return true;
        } else {
            return false;
        }
    }
}
