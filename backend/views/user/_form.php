<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->input('email', ['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Leave this field blank if you do not want to change the password', 'minlength' => 6]) ?>

    <?php if (Yii::$app->user->identity->role == User::ROLE_ADMIN) :?>

    <?= $form->field($model, 'role')->dropDownList(User::getRoleAsArray()); ?>

    <?php endif;?>

    <?= $form->field($model, 'status')->dropDownList(\common\models\User::getStatusAsArray()) ?>

    <?= $form->field($model, 'dob')->textInput(['placeholder' => 'Date format : yyyy-mm-dd (eg. 1980-12-24)']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>