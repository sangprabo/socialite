<?php

namespace frontend\controllers;

use Yii;
use common\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
 * ProfileController handles profile page and profile update
 */
class ProfileController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'delete-picture'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action)
                        {
                            return 
                                Yii::$app->user->identity['role'] == User::ROLE_ADMIN ||
                                Yii::$app->user->identity['role'] == User::ROLE_USER;
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete-picture' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * View user's profile
     * @return mixed
     */
    public function actionIndex()
    {
        $user = $this->findModel(Yii::$app->user->id);

        return $this->render('index', [
            'model' => $user,
        ]);
    }

    /**
     * Updates user's profile
     * If update is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->user->id);

        if (Yii::$app->request->post())
        {

            $post_user = Yii::$app->request->post('User');

            $model->username = $post_user['username'];
            $model->name     = $post_user['name'];
            $model->surname  = $post_user['surname'];
            $model->dob      = $post_user['dob'];

            // update password
            if ( ! empty($post_user['password']))
                $model->setPassword($post_user['password']);

            if ($model->save())
            {
                // save uploaded file
                $model->picture = UploadedFile::getInstance($model, 'picture');
                if ($model->picture)
                {
                    $old_filename = $model->picture_filename;
                    $model->picture_filename = Yii::$app->security->generateRandomString() . '.' . $model->picture->extension;
                    $model->save();
                    
                    if ($model->upload($model->picture_filename))
                    {
                        if ( ! empty($old_filename))
                            unlink(Yii::$app->basePath . '/web/uploads/' . $old_filename);
                    }
                    else 
                    {
                        $model->picture_filename = null;
                        $model->save();
                    }
                }
                return $this->redirect(['index']);
            }
            else 
                return $this->render('update', [
                    'model' => $model,
                ]);
        } 
        else 
        {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Delete user's profile picture
     * @return mixed
     */
    public function actionDeletePicture()
    {
        $model = $this->findModel(Yii::$app->user->id);
       
        if ( ! empty($model->picture_filename))
        {
            unlink(Yii::$app->basePath . '/web/uploads/' . $model->picture_filename);
            $model->picture_filename = null;    
            $model->save();
        }

        return $this->redirect(['index']);
    }


    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
