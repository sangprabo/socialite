<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $name;
    public $surname;
    public $email;
    public $password;
    public $dob;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // ['username', 'filter', 'filter' => 'trim'],
            // ['username', 'required'],
            // ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            // ['username', 'string', 'min' => 2, 'max' => 255],
            
            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'required'],
            ['name', 'string', 'max' => 255, 'min' => 3],

            ['surname', 'filter', 'filter' => 'trim'],
            ['surname', 'required'],
            ['surname', 'string', 'max' => 255, 'min' => 3],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['dob', 'required'],
            ['dob', 
                'date', 
                'format'  => 'yyyy-M-d', 
                'max'     => date('Y-m-d', strtotime('-13 year')), 
                'message' => 'The format for Date of Birth is yyyy-mm-dd (eg : 1980-12-25)',
                'tooBig'  => 'Age must be older than 13 years',
            ],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ( ! $this->validate()) {
            return null;
        }
        
        $user          = new User();
        $user->name    = $this->name;
        $user->surname = $this->surname;
        $user->email   = $this->email;
        $user->dob     = $this->dob;

        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generateVerificationCode();

        $user->role   = User::ROLE_USER;
        $user->status = User::STATUS_PENDING;

        return $user->save() ? $user : null;
    }

    /**
     * Sends an email with a link, to activate the account
     *
     * @return boolean whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_PENDING,
            'email' => $this->email,
        ]);

        if ( ! $user) {
            return false;
        }
        
        if (!$user->save()) {
            return false;
        }

        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerification-html', 'text' => 'emailVerification-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Please activate your account on ' . Yii::$app->name)
            ->send();
    }
}
