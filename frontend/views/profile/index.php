<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Profile Page';
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete Picture'), ['delete-picture'], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete your profile picture?'),
                'method' => 'post',
            ],
        ]) ?>
        
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'username',
            'name',
            'surname',
            'email',
            [
                'attribute' => 'status',
                'value' => User::getStatusAsArray()[$model->status]
            ],
            'dob',
            [
                'attribute' => 'picture_filename',
                'value' => empty($model->picture_filename) ? 'No Picture' : Html::img('/uploads/' . $model->picture_filename, ['width' => '300px']),
                'format' => 'raw',
            ]
        ],
    ]) ?>

</div>